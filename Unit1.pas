unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.JSON, uniGUIBaseClasses,
  uniGUIClasses, uniButton;

type
  TForm1 = class(TForm)
    UniButton1: TUniButton;
    procedure UniButton1Click(Sender: TObject);
  private
    SendMMF: THandle;
    SendData: PChar;
    function GetJson: string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.UniButton1Click(Sender: TObject);
var
    SecurityAttr: TSecurityAttributes;
    SecurityDescr: TSecurityDescriptor;
begin
    InitializeSecurityDescriptor(@SecurityDescr,SECURITY_DESCRIPTOR_REVISION);
    SetSecurityDescriptorDacl(@SecurityDescr,True,nil,False);
    SecurityAttr.nLength:=SizeOf(TSecurityAttributes);
    SecurityAttr.lpSecurityDescriptor:=@SecurityDescr;
    SecurityAttr.bInheritHandle:=True;

    SendMMF := CreateFileMapping($FFFFFFFF, @SecurityAttr, PAGE_READWRITE, 0, 32,PChar('Global\Test'));

    if (SendMMF = 0) then
     Exit;

    SendData := MapViewOfFile(SendMMF, FILE_MAP_WRITE, 0, 0, 0);

    if SendData = nil then
        Exit;


    StrPCopy(SendData, GetJson);

    if Assigned(SendData) then
        Exit;

    if SendMMF.Size > 0 then
        Exit;

    UnmapViewOfFile(SendData);
    SendData := nil;
    CloseHandle(SendMMF);
end;


function TForm1.GetJson: string;
var
    JsonPair: TJSONPair;
    JsonObject: TJSONObject;
    JsonArray: TJSONArray;
    str: string;
begin
    JsonArray := TJSONArray.Create();
    JsonPair := TJSONPair.Create('Name', jsonArray);
    JsonObject := TJSONObject.Create();

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonObject := TJSONObject.Create();
    JsonObject.AddPair(TJSONPair.Create('row', '2'));
    JsonObject.AddPair(TJSONPair.Create('column', '2'));
    JsonObject.AddPair(TJSONPair.Create('value', 'value'));
    JsonObject.AddPair(TJSONPair.Create('border', 'true'));
    JsonObject.AddPair(TJSONPair.Create('bold', 'true'));
    JsonArray.AddElement(JsonObject);

    JsonArray.AddElement(JsonObject);

    Result := JsonArray.ToJSON;
end;

end.
